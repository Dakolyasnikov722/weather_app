import 'package:flutter/material.dart';
import 'package:weather_app/screens/CityScreen.dart';
import 'package:weather_app/screens/LoaderScreen.dart';
import 'package:weather_app/screens/WeatherScreen.dart';

class WeatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (ctx, ch) {
        return Theme(
            data: ThemeData.dark().copyWith(
                appBarTheme:
                    AppBarTheme(elevation: 0, color: Colors.transparent)),
            child: ch);
      },
      routes: {
        '/': (ctx) => LoaderScreen(),
        '/weather/': (ctx) => WeatherScreen(),
        '/city/': (ctx) => CityScreen(),
      },
    );
  }
}
