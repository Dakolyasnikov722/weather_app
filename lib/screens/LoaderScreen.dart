import 'package:flutter/material.dart';
import 'package:weather_app/services/RootService.dart';

class LoaderScreen extends StatefulWidget {
  @override
  _LoaderScreenState createState() => _LoaderScreenState();
}

class _LoaderScreenState extends State<LoaderScreen> {
  @override
  void initState() {
    super.initState();
    startServiceInit();
  }

  startServiceInit() {
    RootService().init().then(afterServiceInit);
  }

  afterServiceInit(empty) {
    RootService().weatherService.loadWeather();
    Navigator.of(context).pushReplacementNamed('/weather/');
    Navigator.of(context).pushNamed('/city/');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
