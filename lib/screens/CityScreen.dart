import 'package:flutter/material.dart';
import 'package:weather_app/models/CityModel.dart';
import 'package:weather_app/services/RootService.dart';

class CityScreen extends StatelessWidget {
  final ValueNotifier<List> filteredCities = ValueNotifier<List>(List());
  onFilter(String query) {
    filteredCities.value = RootService().cityStore.filter(query);
  }

  _onCityChoose(context, String city) async{
    RootService().weatherService.currentCity.value = CityModel(city);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Выбор города'),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16),
            child: TextField(
              onChanged: onFilter,
            ),
          ),
          Expanded(
            child: ValueListenableBuilder(
              valueListenable: filteredCities,
              builder: (context, data, child) {
                if (data == null || data.length == 0) {
                  data = RootService().cityStore.filter(null);
                }
                return ListView(
                  children: List.generate(data.length, (i) {
                    var item = data[i];
                    return ListTile(
                      title: Text(item),
                      onTap: () => _onCityChoose(context, item),
                    );
                  }),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
