import 'package:animated/animated.dart';

import 'package:flutter/material.dart';
import 'package:weather_app/models/CityModel.dart';
import 'package:weather_app/models/WeatherResultModel.dart';
import 'package:weather_app/services/RootService.dart';
import 'package:weather_app/widgets/WeatherWidget.dart';

class WeatherScreen extends StatelessWidget {
  _changeCity(context) {
    Navigator.of(context).pushNamed('/city/');
  }

  _refreshData() {
    RootService().weatherService.loadWeather();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.location_city),
            onPressed: () => _changeCity(context)),
        actions: <Widget>[
          ValueListenableBuilder(
              valueListenable: RootService().weatherService.inLoad,
              builder: (ctx, bool inLoad, child) {
                if (RootService().weatherService.currentCity.value == null) {
                  return Container();
                }

                return Animated(
                  value: inLoad ? 1.0 : 0,
                  curve: Curves.easeOut,
                  duration: Duration(seconds: 10),
                  child: IconButton(
                    icon: Icon(Icons.refresh),
                    onPressed: _refreshData,
                  ),
                  builder: (context, child, animation) => Transform.rotate(
                    angle: inLoad  ?animation.value * 30: 0,
                    child: child,
                  ),
                );
              })
        ],
        centerTitle: true,
        title: ValueListenableBuilder(
            valueListenable: RootService().weatherService.currentCity,
            builder: (context, CityModel data, child) =>
                data == null || data.city == null
                    ? Text('Погода')
                    : Text(data.city)),
      ),
      body: ValueListenableBuilder(
          valueListenable: RootService().weatherService.currentWeather,
          builder: (context, WeatherResultModel data, child) {
            return WeatherWidget(weatherResultModel: data);
          }),
    );
  }
}
