import 'dart:convert';

enum WeatherResultType { success, apiFail, cityFail }

class WeatherResultModel {
  double temp;
  int humidity;
  int pressure;
  int clouds;
  double get mmhg{
    return pressure * 0.75006375541921;
  }
  int get cels{
    if(temp == null){
      return null;
    }
    return (temp - 273.15).floor();
  }
  WeatherResultType type;
  static WeatherResultModel failNoCity() {
    WeatherResultModel impl = WeatherResultModel();
    impl.type = WeatherResultType.cityFail;
    return impl;
  }

  static WeatherResultModel failApi() {
    WeatherResultModel impl = WeatherResultModel();
    impl.type = WeatherResultType.apiFail;
    return impl;
  }

  static fromJson(String json) {
    var parsedJson = jsonDecode(json);
    WeatherResultModel item = WeatherResultModel();
    if (parsedJson != null && parsedJson['main'] != null) {
      item.temp = parsedJson['main']['temp'];
      item.humidity = parsedJson['main']['humidity'];
      item.pressure = parsedJson['main']['pressure'];
      
      if (parsedJson['clouds'] != null && parsedJson['clouds']['all'] != null) {
        item.clouds = parsedJson['clouds']['all'];
        item.type = WeatherResultType.success;
        return item;
      }
    }
    return WeatherResultModel.failApi();
  }
}
