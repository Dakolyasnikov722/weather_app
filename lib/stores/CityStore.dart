import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

List parseJson(String rawJson) {
  return jsonDecode(rawJson) as List;
}

class CityStore {
  List cities = List();
  load() async {
    var rawJson = await rootBundle.loadString('res/cities.json');
    compute(parseJson, rawJson).then(_onParseDone);
  }

  _onParseDone(List jsonList) {
    cities = jsonList;
  }

  List filter(String query) {
    if (query == null || query.length < 1) {
      return [
        'Москва',
        'Санкт-Петербург',
        'Екатеринбург',
        'Новосибирск',
        'Тюмень',
        'Омск',
        'Красноярск',
        'Владивосток',
      ];
    }
    List citis = cities
        .where((f) => f.toLowerCase().startsWith(query.toLowerCase()))
        .toList();
    print(citis);
    return citis;
  }
}
