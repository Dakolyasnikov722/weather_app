import 'package:weather_app/services/ApiService.dart';
import 'package:weather_app/services/IService.dart';
import 'package:weather_app/services/WeatherService.dart';
import 'package:weather_app/stores/CityStore.dart';

class RootService implements IService {
  // синглтон
  static final RootService _rootService = RootService._internal();
  RootService._internal();
  factory RootService() {
    return _rootService;
  }

  // сервисы
  WeatherApiService api;
  WeatherService weatherService;
  

  // Сторы
  CityStore cityStore;

  @override
  Future init() async {
    api = WeatherApiService('http://api.openweathermap.org/data/2.5/');
    weatherService = WeatherService(api);
    cityStore = CityStore();
    cityStore.load();
    await api.init();
    await weatherService.init();
    return null;
  }
}
