import 'package:flutter/cupertino.dart';
import 'package:weather_app/models/CityModel.dart';
import 'package:weather_app/models/WeatherResultModel.dart';
import 'package:weather_app/services/ApiService.dart';
import 'package:weather_app/services/IService.dart';

class WeatherService implements IService {
  final WeatherApiService apiService;
  ValueNotifier<WeatherResultModel> currentWeather;
  ValueNotifier<CityModel> currentCity;
  ValueNotifier<bool> inLoad;
  WeatherService(this.apiService);
  @override
  Future init() {
    currentWeather = ValueNotifier<WeatherResultModel>(null);
    currentCity = ValueNotifier<CityModel>(null);
    currentCity.addListener(_cityChanged);
    inLoad = ValueNotifier(false);
    return null;
  }

  loadWeather() async {
    inLoad.value = true;
    if (currentCity == null || currentCity.value == null) {
      currentWeather.value = WeatherResultModel.failNoCity();
      return;
    }
    apiService.loadWeather(currentCity.value.city).then(_weatherLoaded);
  }

  _cityChanged() {
    loadWeather();
  }

  _weatherLoaded(WeatherResultModel result) async {
    currentWeather.value = result;
    inLoad.value = false;
  }
}
