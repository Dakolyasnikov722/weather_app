import 'package:weather_app/models/WeatherResultModel.dart';
import 'package:weather_app/services/IService.dart';
import 'package:http/http.dart' as http;

class WeatherApiService implements IService {
  final String base;
  http.Client _client;
  final String _apiKey = '3ccb01be19572da8d36f847f325ab97f';
  WeatherApiService(this.base);

  @override
  Future init() {
    _client = http.Client();
    return null;
  }

  Future<WeatherResultModel> loadWeather(String city) async {
    await Future.delayed(Duration(milliseconds: 500));
    http.Response response;
    if (city == null) {
      return WeatherResultModel.failNoCity();
    }
    try {
      response = await _client.get(base + "weather?q=$city&appid=$_apiKey");
      if (response.statusCode != 200) {
        return WeatherResultModel.failApi();
      }
    } catch (e) {
      return WeatherResultModel.failApi();
    }

    return WeatherResultModel.fromJson(response.body);
  }
}
