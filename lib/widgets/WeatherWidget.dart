import 'package:flutter/material.dart';
import 'package:weather_app/models/WeatherResultModel.dart';

class WeatherWidget extends StatelessWidget {
  final WeatherResultModel weatherResultModel;

  String get pressureDisplay {
    return weatherResultModel.mmhg.toString().substring(0, 6);
  }

  String get humidity {
    return weatherResultModel.humidity.toString() + "%";
  }

  int get clouds {
    return weatherResultModel.clouds;
  }

  Color get color{
    if(weatherResultModel.cels == null){
      return Colors.grey[900];
    }
    double mix = ((weatherResultModel.cels+40)/80)+(weatherResultModel.cels > 0? 0.1:-0.2);

    return Color.lerp(Colors.blue,Colors.orange ,mix );

  }

  const WeatherWidget({Key key, @required this.weatherResultModel})
      : super(key: key);
  @override
  Widget build(BuildContext context){
    return Container(
      color: color,
      child: buildBody(context)
    );
  }
  Widget buildBody(BuildContext context) {
    if (weatherResultModel == null) {
      return Center(child: Text('Нет данных о погоде'));
    }
    if (weatherResultModel.type == WeatherResultType.cityFail) {
      return Center(child: Text('Не выбран город'));
    }
    if (weatherResultModel.type == WeatherResultType.apiFail) {
      return Center(child: Text('Ошибка при получении данных'));
    }
    if (weatherResultModel.type == WeatherResultType.success) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 60.0),
              child: Text(
                weatherResultModel.cels.toString() + " °C",
                style: TextStyle(fontSize: 60, fontWeight: FontWeight.w200),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  WeatherSecondLine(
                    display: pressureDisplay,
                    subdisplay: 'мм рт.ст.',
                  ),
                  WeatherSecondLine(
                    display: humidity,
                    subdisplay: 'влажность',
                  ),
                  SunDisplay(
                    clouds: clouds,
                  )
                ],
              ),
            )
          ],
        ),
      );
    }
    return Center(child: Text('Нет данных о погоде'));
  }
}

class SunDisplay extends StatelessWidget {
  final int clouds;

  IconData get icon {
    if (clouds > 80) {
      return Icons.cloud;
    }
    return Icons.wb_sunny;
  }

  String get title {
    if (clouds > 80) {
      return "Облачно";
    }
    return "Солнечно";
  }

  const SunDisplay({Key key, this.clouds}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Icon(icon, size: 26,),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text(
              title.toLowerCase(),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w100),
            ),
          )
        ],
      ),
    );
  }
}

class WeatherSecondLine extends StatelessWidget {
  final String display;
  final String subdisplay;

  const WeatherSecondLine({Key key, this.display, this.subdisplay})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          display.toString().toLowerCase(),
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.w200),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(
            subdisplay.toString().toLowerCase(),
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w100),
          ),
        ),
      ],
    );
  }
}
